#include <iostream>
#include <cstdlib>
#include <cmath>

using namespace std;

int main(int argc, char const *argv[])
{
	int n;
	int m;
	bool fn, fm;
	cin >> n >> m;
	if(n < 1){
		cout << "UNDECIDABLE";
		exit(1);
	}
	if(m > pow(10,4)){
		cout << "UNDECIDABLE";
		exit(1);
	}
	for (int i = 1; i < 2147483647; ++i)
	{
		fn = 0;
		fm = 0;
		if(i%n == 0){
			fn = 1;
		}
		if(i%m == 0){
			fm = 1;
		}
		if(fn == 1 && fm == 1){
			cout << i;
			break;
		}
	}
	return 0;
}

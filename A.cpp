#include <iostream>
#include <cstdlib>

using namespace std;

int main(int argc, char const *argv[])
{
	int a, b, c;
	cin >> a >> b >> c;
	if(a < 0){
		cout << "UNDECIDABLE";
		exit(1);
	}
	if(c > 1000){
		cout << "UNDECIDABLE";
		exit(1);
	}
	cout << c + b + c;
	
}

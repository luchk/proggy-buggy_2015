#include <iostream>
#include <cstdlib>

using namespace std;
bool is_prime(int n);
int main(int argc, char const *argv[])
{
	int n;
	cin >> n;
	if(n > 100){
		cout << "UNDECIDABLE";
		exit(0);
	}
	if(n < 3){
		cout << "UNDECIDABLE";
		exit(0);
	}
	if (is_prime(n) == 0){
		for (int i = 0; i < n; ++i){
			for (int j = 0; j < n; ++j)
			{
				cout << i << " ";
			}
			cout << "\n";
		}
	}else{
		cout << "UNDECIDABLE";
	}
	return 0;
}


bool is_prime(int n)
{
  bool flag = false;

  for(int i = 2; i <= n/2; ++i)
  {
      if(n%i == 0)
      {
          flag = true;
          break;
      }
  }
  return flag;
}